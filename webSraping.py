import pandas as pd
import base64
from bs4 import BeautifulSoup
import os
from collections import Counter
import requests

# Extract the second col in the csv
class HTMLTitleExtractor:
    def __init__(self):
        self.list_of_words = []
        self.date_list=[]
    def title_extract(self, data):
        # Extract title of html that coded by base64 step by step
        for row in range(0, len(data)):
            coded_string = data.iloc[row, 1]
            html_string = base64.b64decode(coded_string).decode('UTF-8')
            soup = BeautifulSoup(html_string, 'html.parser')
            clean_text = getattr(soup.find('title'), 'text', None)
            # Some parts do not have a title option
            if clean_text is not None:
                clean_split = clean_text.split()
                self.list_of_words.extend(clean_split)  # Add multiple lists to a single list
            else:
                continue
        self.clean_wordlist()  # Calling the clean_wordlist method

    def clean_wordlist(self):
        clean_list = []
        symbols = "!@#$%^&*()_-+={[}]|;:\"<>?/., "

        for word in self.list_of_words:
            for symbol in symbols:
                word = word.replace(symbol, '')
            if len(word) > 0:
                clean_list.append(word)

        self.filter_list(clean_list)

    def filter_list(self, clean_list):
        word_count = {}

        for word in clean_list:
            if word in word_count:
                word_count[word] += 1
            else:
                word_count[word] = 1

        c = Counter(word_count)
        top = c.most_common(10)
        df = pd.DataFrame(top, columns=('trend', 'counting'))
        print(df)
        
# Extract the first col in the csv
class URLExtractor:
    def __init__(self):
        self.list_of_words = []

    def title_extract(self, data):
        # Extract title of HTML from URLs that are in the first column of the CSV
        for row in range(0, len(data)):
            url = data.iloc[row, 0] 
            response = requests.get(url)
            if response.status_code == 200:
                html_content = response.text
                soup = BeautifulSoup(html_content, 'html.parser')
                title = soup.find('title')
                if title and title.text:  # Check if title is not None and not an empty string
                    clean_split = title.text.split()
                    self.list_of_words.extend(clean_split)  # Add multiple lists to a single list
            else:
                continue

        self.clean_wordlist()  # Calling the clean_wordlist method

    def clean_wordlist(self):
        clean_list = []
        symbols = "!@#$%^&*()_-+={[}]|;:\"<>?/., "

        for word in self.list_of_words:
            for symbol in symbols:
                word = word.replace(symbol, '')
            if len(word) > 0:
                clean_list.append(word)

        self.filter_list(clean_list)

    def filter_list(self, clean_list):
        word_count = {}

        for word in clean_list:
            if word in word_count:
                word_count[word] += 1
            else:
                word_count[word] = 1

        c = Counter(word_count)
        top = c.most_common(10)
        df = pd.DataFrame(top, columns=('trend', 'counting'))
        print(df)

    

   

path = '/home/nazanin/Desktop/New Folder'
dirs = os.listdir(path)

for file in dirs:
    if file.endswith('.csv'):  # Check if the file ends with .csv
        print('Extracting html from file: ' + file)
        full_file_path = os.path.join(path, file)  # Full path to the CSV file
    
        data = pd.read_csv(full_file_path)   # Read the CSV file once
        
        html_extractor = HTMLTitleExtractor()
        html_extractor.title_extract(data)

        print('Extracting url from file: ' + file)
        url_extractor=URLExtractor()
        url_extractor.title_extract(data)
     
